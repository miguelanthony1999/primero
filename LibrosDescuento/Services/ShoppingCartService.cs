﻿using LibrosDescuento.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LibrosDescuento.Services
{
    public class ShoppingCartService
    {
        private readonly List<Book> shoppingCart;

        public ShoppingCartService()
        {
            shoppingCart = new List<Book>();
        }

        public void AddBookToCart(Book book)
        {
            shoppingCart.Add(book);
        }

        public void RemoveBookFromCart(int bookId)
        {
            var book = shoppingCart.FirstOrDefault(o => o.Id == bookId);
            if (book != null)
                shoppingCart.Remove(book);
        }

        public decimal GetTotal()
        {
            return shoppingCart.Sum(o => o.Price);

        }
        public List<Book> GetShoppingCart()
        {
            return shoppingCart;
        }
        public void ClearShoppingCart()
        {
            shoppingCart.Clear();
        }
        public bool EsSerieCart()
        {
            return true;
        }

        public decimal descuentoCart()
        {
            decimal go = 0.0m;
            var result = shoppingCart.Where(o => o.SerieId == 1).ToList();
            int contador = result.Count();
            if (contador >= 2)
            {
                go = result.Sum(o => o.Price * 0.1m);
            }
            else
            {
                go = result.Sum(o => o.Price);
            }

            return go;
        }
    }
}
