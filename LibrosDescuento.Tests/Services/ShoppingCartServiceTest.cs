﻿using LibrosDescuento.Entities;
using LibrosDescuento.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibrosDescuento.Tests.Services
{
    [TestFixture]
    public class ShoppingCartServicesTest
    {
        [Test]
        public void GetTotalCaso1()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "Harry Potter y la Piedra filosofal", Price = 10.5m });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "Harry Potter y la Camara de los secretos", Price = 12.5m });
            shoppingCart.AddBookToCart(new Book { Id = 3, Name = "Harry Potter y El prisionero de Azkaban", Price = 15.0m });
            var result = shoppingCart.GetTotal();
            Assert.AreEqual(38.0m, result);
        }

        [Test]
        public void GetTotalCaso2()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "Harry Potter y la Piedra filosofal", Price = 10.5m });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "Harry Potter y la Camara de los secretos", Price = 12.5m });
            var result = shoppingCart.GetTotal();
            Assert.AreEqual(23.0m, result);
        }

        [Test]
        public void GetTotalCaso3()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "Harry Potter y la Piedra filosofal", Price = 10.5m });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "Harry Potter y la Camara de los secretos", Price = 12.5m });
            shoppingCart.AddBookToCart(new Book { Id = 3, Name = "Harry Potter y El prisionero de Azkaban", Price = 15.0m });
            shoppingCart.RemoveBookFromCart(3);
            var result = shoppingCart.GetTotal();
            Assert.AreEqual(23.0m, result);
        }

        [Test]
        public void GetTotalCaso4()
        {
            var shoppingCart = new ShoppingCartService();
            var result = shoppingCart.GetTotal();
            Assert.AreEqual(0m, result);
        }

        [Test]
        public void EsSerieCaso1()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "Harry Potter y la Piedra filosofal", Price = 10.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "Harry Potter y la Camara de los secretos", Price = 12.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 3, Name = "Harry Potter y El prisionero de Azkaban", Price = 15.0m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 4, Name = "Juego de Tronos", Price = 15.0m, SerieId = 2 });

            var a = shoppingCart.EsSerieCart();
            Assert.AreEqual(a, true);
        }

        [Test]
        public void AddBookToCartCaso1()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "Libro 1", Price = 10.0m });
            var result = shoppingCart.GetShoppingCart().Count;
            Assert.AreEqual(1, result);
        }

        [Test]
        public void AddBookToCartCaso2()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "Libro 2", Price = 10.0m });
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "Libro 3", Price = 10.0m });
            var result = shoppingCart.GetShoppingCart().Count;
            Assert.AreEqual(2, result);
        }

        [Test]
        public void ClearCartCaso1()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "Libro 2", Price = 10.0m });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "Libro 3", Price = 10.0m });

            shoppingCart.ClearShoppingCart();
            var result = shoppingCart.GetShoppingCart().Count;
            Assert.AreEqual(0, result);
        }

        [Test]
        public void RemoveFromCartCaso1()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "Libro 1", Price = 10.0m });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "Libro 2", Price = 10.0m });
            shoppingCart.AddBookToCart(new Book { Id = 3, Name = "Libro 3", Price = 10.0m });

            Assert.AreEqual(3, shoppingCart.GetShoppingCart().Count);
            shoppingCart.RemoveBookFromCart(2);
            Assert.AreEqual(2, shoppingCart.GetShoppingCart().Count);

            var a = shoppingCart.GetShoppingCart().Count;
        }

        [Test]
        public void DescuentoCartCaso1()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "Harry Potter y las reliquias de la muerte", Price = 10.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "Harry Potter y el principe mestizo", Price = 12.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 3, Name = "Harry Potter y la pieda filosofal", Price = 15.0m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 4, Name = "The Walking Dead", Price = 15.0m, SerieId = 2 });;

            Assert.AreEqual(68, shoppingCart.descuentoCart());
        }

        [Test]
        public void DescuentoCartCaso2()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "Cronicas de Narnia1", Price = 10.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "Cronicas de Narnia2", Price = 15.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "Cronicas de Narnia3", Price = 15.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 3, Name = "Js", Price = 15.0m, SerieId = 2 });


            Assert.AreEqual(4.15, shoppingCart.descuentoCart());
        }

        [Test]
        public void DescuentoCartCaso3()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "The Sun", Price = 10.5m, SerieId = 2 });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "Batman", Price = 5.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 3, Name = "The Sun 3", Price = 10.0m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 4, Name = "The Sun 4", Price = 15.0m, SerieId = 1 });
        

            Assert.AreEqual(3.05, shoppingCart.descuentoCart());
        }

        [Test]
        public void DescuentoCartCaso4()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "Iron Man 1", Price = 10.0m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "Iron Man 2", Price = 5.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 3, Name = "Iron Man 3", Price = 10.0m, SerieId = 1 });

            Assert.AreEqual(2.55, shoppingCart.descuentoCart());
        }

        [Test]
        public void DescuentoCartCaso5()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "El Rey León", Price = 10.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "El Rey León 2", Price = 12.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 3, Name = "El Rey León 3", Price = 15.0m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 4, Name = "Sing", Price = 15.0m, SerieId = 2 });
            shoppingCart.AddBookToCart(new Book { Id = 5, Name = "El Vagabundo", Price = 15.0m, SerieId = 2 });

            Assert.AreEqual(68, shoppingCart.descuentoCart());
        }

        [Test]
        public void DescuentoCartCaso6()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "SAW 1", Price = 10.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "SAW 2", Price = 15.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 3, Name = "SAW 3", Price = 15.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 5, Name = "WTD", Price = 15.0m, SerieId = 2 });

            Assert.AreEqual(4.15, shoppingCart.descuentoCart());
        }

        [Test]
        public void DescuentoCartCaso7()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "American Horror Story", Price = 10.5m, SerieId = 2 });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "The Church 3", Price = 5.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 3, Name = "The Church 4", Price = 18.0m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 4, Name = "The Church 5", Price = 15.0m, SerieId = 1 });

            Assert.AreEqual(3.05, shoppingCart.descuentoCart());
        }

        [Test]
        public void DescuentoCartCaso8()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "Rápidos y Furiosos 1", Price = 10.0m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "Rápidos y Furiosos 2", Price = 5.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 3, Name = "Rápidos y Furiosos 3", Price = 10.0m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 4, Name = "Rápidos y Furiosos 4", Price = 15.0m, SerieId = 1 });

            Assert.AreEqual(3.05, shoppingCart.descuentoCart());
        }

        [Test]
        public void DescuentoCartCaso9()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "Toy Story 1", Price = 10.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "Toy Story 2", Price = 12.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 3, Name = "Toy Story 3", Price = 15.0m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 4, Name = "The Nun", Price = 15.0m, SerieId = 2 });
            shoppingCart.AddBookToCart(new Book { Id = 5, Name = "El Hobbit", Price = 15.0m, SerieId = 2 });

            Assert.AreEqual(68, shoppingCart.descuentoCart());
        }

        [Test]
        public void DescuentoCartCaso10()
        {
            var shoppingCart = new ShoppingCartService();
            shoppingCart.AddBookToCart(new Book { Id = 1, Name = "El señor de los anillos", Price = 10.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 2, Name = "El señor de los anillos 2", Price = 12.5m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 3, Name = "El señor de los anillos 3", Price = 15.0m, SerieId = 1 });
            shoppingCart.AddBookToCart(new Book { Id = 4, Name = "Alien", Price = 15.0m, SerieId = 2 });
            shoppingCart.AddBookToCart(new Book { Id = 5, Name = "Dumbo", Price = 15.0m, SerieId = 2 });

            Assert.AreEqual(68, shoppingCart.descuentoCart());
        }
    }
}
